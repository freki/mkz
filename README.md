# README - Antivirus #

### About application ###
This repository contains a logical game Antivirus for Android. The goal of the game is get the virus (red piece) out of the cell (play board). If you do so, you win.

**Version:** Current application version is 1.0. 

### SKD ###
The target SDK is Android 5.0, but the minimum is 3.0 and the app was tested on 4.2.2.