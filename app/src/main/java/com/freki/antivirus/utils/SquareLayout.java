package com.freki.antivirus.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Custom layout class which is always square.
 */
public class SquareLayout extends FrameLayout {

    /**
     * Constructor with context.
     *
     * @param context context
     */
    public SquareLayout(Context context) {
        super(context);
    }

    /**
     * Constructor with context and attributes.
     *
     * @param context context
     * @param attrs   layout attributes
     */
    public SquareLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Sets the width and heigth of layout to be square.
     *
     * @param widthMeasureSpec  layout width
     * @param heightMeasureSpec layout height
     */
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //int used = Math.min(widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
