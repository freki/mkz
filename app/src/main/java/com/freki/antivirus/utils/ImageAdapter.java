package com.freki.antivirus.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.freki.antivirus.R;

import java.util.ArrayList;
import java.util.List;

/**
 * User Adapter for GridView class. Used for displaying levels.
 */
public class ImageAdapter extends BaseAdapter {

    /**
     * List with ids of images resources.
     */
    private List<Integer> images = new ArrayList<>(20);
    /**
     * Inflater.
     */
    private static LayoutInflater inflater = null;

    /**
     * Creates an instance of ImageAdapter.
     *
     * @param c context
     */
    public ImageAdapter(Context c) {
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Adds an image to list.
     *
     * @param resource id of the image resource
     */
    public void addImage(Integer resource) {
        images.add(resource);
    }

    /**
     * Returns count of images.
     *
     * @return count of images in list
     */
    public int getCount() {
        return images.size();
    }

    /**
     * Returns object on a specific position.
     * Not implemented.
     */
    public Object getItem(int position) {
        return null;
    }

    /**
     * Returns image resource on a specific position.
     */
    public long getItemId(int position) {
        if (position < images.size())
            return images.get(position);
        return 0;
    }

    /**
     * Gets the view to be shown in the gridView.
     *
     * @param position    position in the gridView
     * @param convertView convert view
     * @param parent      parent view group
     * @return view to be shown
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();

        View rowView;
        rowView = inflater.inflate(R.layout.level, null);
        holder.tv = (TextView) rowView.findViewById(R.id.textView1);
        holder.img = (ImageView) rowView.findViewById(R.id.imageView1);

        holder.tv.setText("Level " + (position + 1));

        holder.img.setScaleType(ImageView.ScaleType.FIT_CENTER);
        holder.img.setPadding(0, 0, 0, 0);

        holder.img.setImageResource(images.get(position));
        return rowView;
    }

    /**
     * Holder class representing ImageView with text.
     */
    private class Holder {
        TextView tv;
        ImageView img;
    }
}
