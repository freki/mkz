package com.freki.antivirus.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.MotionEvent;
import android.view.View;

import com.freki.antivirus.R;
import com.freki.antivirus.gameLogic.Board;
import com.freki.antivirus.gameLogic.Piece;

/**
 * TouchListener class for piece movements.
 */
public class MyTouchListener implements View.OnTouchListener {
    /**
     * X-coordinate of first touch.
     */
    private float x;
    /**
     * Y-coordinate of first touch.
     */
    private float y;

    /**
     * Handles the onTouch events.
     *
     * @param v     View which was touch
     * @param event MotionEvent instance that holds information about event
     * @return true if event was handled, false otherwise
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x = event.getX();
                y = event.getY();
                return true;
            case MotionEvent.ACTION_MOVE:
                if (Board.won)
                    return true;
                if (Math.abs(x - event.getX()) > Math.abs(y - event.getY())) {
                    ((Piece) v).move(event.getX() - x, 0);
                } else {
                    ((Piece) v).move(0, event.getY() - y);
                }
                return true;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (Board.won) {
                    Dialog d = new WonDialog((Activity) v.getContext());
                    d.setTitle(R.string.won);
                    d.show();
                }
                return true;
        }
        return false;
    }
}
