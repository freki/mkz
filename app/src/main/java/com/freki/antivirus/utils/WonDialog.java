package com.freki.antivirus.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;

import com.freki.antivirus.R;
import com.freki.antivirus.view.Game;
import com.freki.antivirus.view.GameSelect;

/**
 * Represents the alert dialog shown after end of level.
 */
public class WonDialog extends Dialog implements View.OnClickListener {
    /**
     * Buttons in the dialog.
     */
    ImageButton btnMenu, btnBack, btnNext;
    /**
     * Game activity.
     */
    Activity mActivity;

    /**
     * Creates the dialog.
     *
     * @param activity game activity
     */
    public WonDialog(Activity activity) {
        super(activity);
        mActivity = activity;
        setContentView(R.layout.won_dialog);
        btnMenu = (ImageButton) findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(this);
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        btnNext = (ImageButton) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
    }

    /**
     * On click listener. Reacts on dialog's buttons clicks.
     *
     * @param v button which was clicked on
     */
    @Override
    public void onClick(View v) {
        if (v == btnBack) {
            ((Game) mActivity).restart(v);
            dismiss();
        } else if (v == btnNext) {
            ((Game) mActivity).nextLevel(v);
            dismiss();
        } else {
            Intent i = new Intent(mActivity, GameSelect.class);
            mActivity.startActivity(i);
        }
    }
}
