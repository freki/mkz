package com.freki.antivirus.gameLogic;

import android.content.Context;
import android.widget.ImageView;

/**
 * Class represents one piece on the game board. It can move on users touch.
 */
public class Piece extends ImageView {
    /**
     * Code representing piece in the board.
     */
    private char code;
    /**
     * Board coordinates of all parts of the piece.
     */
    private Coords[] coordinates;
    /**
     * X-coordinate of the most left top part. Used for atypical pieces.
     */
    private float midX;
    /**
     * Y-coordinate of the most left top part. Used for atypical pieces.
     */
    private float midY;

    /**
     * Creates an instance of piece with given context.
     *
     * @param context context
     */
    public Piece(Context context) {
        super(context);
        midX = 0;
        midY = 0;
    }

    /**
     * Returns code of piece.
     *
     * @return piece code
     */
    public char getCode() {
        return code;
    }

    /**
     * Sets the code of piece.
     *
     * @param code new code
     */
    public void setCode(char code) {
        this.code = code;
    }

    /**
     * Returns the board coordinates of all piece parts.
     *
     * @return coordinates
     */
    public Coords[] getCoordinates() {
        return coordinates;
    }

    /**
     * Sets the coordinates for the piece.
     *
     * @param coordinates coordinates to be set
     */
    public void setCoordinates(Coords[] coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * Gets the x-coordinate of the most left top piece part.
     *
     * @return x-coordinate
     */
    public float getMidX() {
        return midX;
    }

    /**
     * Sets the x-coordinate of the most left top piece part.
     *
     * @param midX x-coordinate
     */
    public void setMidX(float midX) {
        this.midX = midX;
    }

    /**
     * Gets the y-coordinate of the most left top piece part.
     *
     * @return y-coordinate
     */
    public float getMidY() {
        return midY;
    }

    /**
     * Sets the y-coordinate of the most left top piece part.
     *
     * @param midY y-coordinate
     */
    public void setMidY(float midY) {
        this.midY = midY;
    }

    /**
     * Moves the piece according user interaction.
     * First checks if all the board fields are free and the piece
     * can be moved, then ask the Board to move piece.
     *
     * @param distX distance moved in x direction
     * @param distY distance moved in y direction
     */
    public void move(float distX, float distY) {
        char[][] field = Board.getField();
        float distance = Board.PIECE_SIZE * 0.51f;
        int moveX = (int) (distX / distance);
        int moveY = (int) (distY / distance);
        boolean canMove = true;

        if (getRotation() == 90) {
            int tmp = moveX;
            moveX = -moveY;
            moveY = tmp;
        } else if (getRotation() == -90) {
            int tmp = moveX;
            moveX = moveY;
            moveY = -tmp;
        } else if (getRotation() == 180) {
            moveX = -moveX;
            moveY = -moveY;
        }

        if (moveX > 0) {
            for (Coords c : coordinates)
                if (c.j + 1 >= Board.SIZE || !(field[c.i][c.j + 1] == Board.FREE || field[c.i][c.j + 1] == code)) {
                    canMove = false;
                    break;
                }
            if (canMove)
                Board.movePiece(this, 0, 1);  // move right
        } else if (moveX < 0) {
            for (Coords c : coordinates)
                if (c.j - 1 < 0 || !(field[c.i][c.j - 1] == Board.FREE || field[c.i][c.j - 1] == code)) {
                    canMove = false;
                    break;
                }
            if (canMove)
                Board.movePiece(this, 0, -1);   // move left
        } else if (moveY > 0) {
            for (Coords c : coordinates)
                if (c.i + 1 >= Board.SIZE || !(field[c.i + 1][c.j] == Board.FREE || field[c.i + 1][c.j] == code)) {
                    canMove = false;
                    break;
                }
            if (canMove)
                Board.movePiece(this, 1, 0);   // move down
        } else if (moveY < 0) {
            for (Coords c : coordinates)
                if (c.i - 1 < 0 || !(field[c.i - 1][c.j] == Board.FREE || field[c.i - 1][c.j] == code)) {
                    canMove = false;
                    break;
                }
            if (canMove)
                Board.movePiece(this, -1, 0);    // move up
        }
    }
}
