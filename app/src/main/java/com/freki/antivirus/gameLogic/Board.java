package com.freki.antivirus.gameLogic;

/**
 * Class representing playing board.
 * The size is fixed - 7x7 fields. Field size is computed according
 * resolution.
 */
public class Board {
    /**
     * Size of board.
     */
    static final int SIZE = 7;
    /**
     * Character representing free field.
     */
    static final char FREE = 'F';
    /**
     * Character representing the wall.
     */
    static final char WALL = 'W';

    /**
     * Boolean value, if the winning condition where met. Is true, if player won.
     */
    public static boolean won;

    /**
     * Size of one field.
     */
    public static int PIECE_SIZE;

    /**
     * Property representing playing board.
     */
    private static char[][] field;

    static {
        PIECE_SIZE = 50;
        field = new char[SIZE][SIZE];
    }

    /**
     * Initialization.
     * Creates the starting board with no pieces in it. Fill the board with free space and walls.
     * Also set won to false.
     */
    public static void initialize() {
        won = false;
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++) {
                if (i == 3)
                    field[i][j] = FREE;
                else if ((i == 2 || i == 4) && (j != 0 && j != 6))
                    field[i][j] = FREE;
                else if ((i == 1 || i == 5) && (j > 1 && j < 5))
                    field[i][j] = FREE;
                else if ((i == 0 || i == 6) && j == 3)
                    field[i][j] = FREE;
                else
                    field[i][j] = WALL;
            }
    }

    /**
     * Returns current board state.
     *
     * @return field representing the board.
     */
    public static char[][] getField() {
        return field;
    }

    /**
     * Sets the field size according the screen resolution.
     *
     * @param size new field size
     */
    public static void setPieceSize(int size) {
        PIECE_SIZE = size;
    }

    /**
     * Adds a piece into the board. Coordinates are specified in the piece.
     *
     * @param imageView piece to be added
     */
    public static void addPiece(Piece imageView) {
        int i = imageView.getCoordinates()[0].i;
        int j = imageView.getCoordinates()[0].j;

        for (Coords c : imageView.getCoordinates()) {
            field[c.i][c.j] = imageView.getCode();
        }

        imageView.setY(i * PIECE_SIZE + imageView.getMidY());
        imageView.setX(j * PIECE_SIZE + imageView.getMidX());

        if (field[0][3] == 'V') {
            won = true;
        }
    }

    /**
     * Moves the specific piece according given difference in the coordinates.
     *
     * @param imageView piece to be moved
     * @param _i        how many rows move piece (if positive move down, if negative move up)
     * @param _j        how many columns move piece (if positive move right, if negative move left)
     */
    public static void movePiece(Piece imageView, int _i, int _j) {
        int i, j;
        for (Coords c : imageView.getCoordinates()) {
            i = c.i;
            j = c.j;
            field[i][j] = FREE;
            c.i = i + _i;
            c.j = j + _j;
        }
        addPiece(imageView);
    }
}
