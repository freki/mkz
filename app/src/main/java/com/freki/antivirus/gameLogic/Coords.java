package com.freki.antivirus.gameLogic;

/**
 * Messenger class to store coordinates of piece part in the field.
 */
public class Coords {

    /**
     * I coordinate - row.
     */
    public int i;
    /**
     * J coordinate - column.
     */
    public int j;

    /**
     * Creates an instance with given coordinates.
     *
     * @param i i coordinate (row)
     * @param j j coordinate (column)
     */
    public Coords(int i, int j) {
        this.i = i;
        this.j = j;
    }
}
