package com.freki.antivirus.view;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.freki.antivirus.R;
import com.freki.antivirus.gameLogic.Coords;
import com.freki.antivirus.gameLogic.Board;
import com.freki.antivirus.gameLogic.Piece;
import com.freki.antivirus.utils.MyTouchListener;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Represents the Game.
 */
public class Game extends Activity {

    /**
     * OnTouch listener for game pieces.
     */
    private MyTouchListener listener;

    /**
     * Number of current level.
     */
    private int curGameNum;
    /**
     * Computed width of one field on board.
     */
    private float width;
    /**
     * List with all pieces currently on the board.
     */
    private List<ImageView> views = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);
        Bundle b = getIntent().getExtras();
        curGameNum = b.getInt("gameNumber");

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        listener = new MyTouchListener();

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels / 7;

      //  width = getWindowManager().getDefaultDisplay().getWidth() / 7;

        Board.setPieceSize((int) width);
        loadGame();
    }

    /**
     * Loads the game. Resource to load from is saved in property currentGame.
     */
    private void loadGame() {
        int count, currentGame;
        Piece imageView;
        InputStream fis;
        Scanner scan;
        FrameLayout layout = (FrameLayout) findViewById(R.id.layout);
        Board.initialize();

        try {
            Class res = R.raw.class;
            Field field = res.getField("level_" + (curGameNum + 1));
            currentGame = field.getInt(null);
        }
        catch (Exception e) {
            currentGame = 0; //start default
        }

        TextView level = (TextView) findViewById(R.id.txtLevel);
        level.setText("Level " + (curGameNum+1));

        try {
            fis = getResources().openRawResource(currentGame);
            scan = new Scanner(fis);
            count = scan.nextInt();
            for (int ii = 0; ii < count; ii++) {
                imageView = setPieceProperties(layout, scan);
                Board.addPiece(imageView);
                this.views.add(imageView);
            }
            scan.close();
            fis.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Resources.NotFoundException e) {
            //load default level ^^
            Coords[] coordinates;
            Piece piece = new Piece(this);
            coordinates = new Coords[2];
            coordinates[0] = new Coords(3, 2);
            coordinates[1] = new Coords(4, 2);

            layout.addView(piece);
            piece.setCoordinates(coordinates);
            piece.setCode('V');
            piece.getLayoutParams().height = (int) width * 2;
            piece.getLayoutParams().width = (int) width;
            piece.setImageResource(R.drawable.antivirus);
            piece.requestLayout();
            piece.setOnTouchListener(listener);
            Board.addPiece(piece);
            this.views.add(piece);

            // add second piece piece
            piece = new Piece(this);
            Coords[] coordinates2 = new Coords[3];
            coordinates2[0] = new Coords(0, 3);
            coordinates2[1] = new Coords(1, 2);
            coordinates2[2] = new Coords(1, 3);

            layout.addView(piece);
            piece.setCoordinates(coordinates2);
            piece.setMidX(-width);
            piece.setCode('d');
            piece.getLayoutParams().height = (int) width * 2;
            piece.getLayoutParams().width = (int) width * 2;
            piece.setImageResource(R.drawable.d);
            piece.requestLayout();
            piece.setOnTouchListener(listener);
            Board.addPiece(piece);
            this.views.add(piece);
        }
    }

    /**
     * Loads ans sets the properties of piece.
     *
     * @param layout layout where piece is placed
     * @param scan   scanner for loading level info
     * @return an instance of Piece class with all properties set
     */
    private Piece setPieceProperties(FrameLayout layout, Scanner scan) {
        char code;
        int pieces;
        Coords[] coordinates;
        int i;
        int j;
        Piece imageView = new Piece(this);
        //load a piece
        code = scan.next().charAt(0);
        pieces = scan.nextInt();
        coordinates = new Coords[pieces];
        for (int k = 0; k < pieces; k++) {
            i = scan.nextInt();
            j = scan.nextInt();
            coordinates[k] = new Coords(i, j);
        }
        layout.addView(imageView);
        imageView.setCoordinates(coordinates);
        imageView.setCode(code);
        switch (code) {
            case 'V':
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width;
                imageView.setImageResource(R.drawable.antivirus);
                break;
            case 'a':
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width;
                imageView.setImageResource(R.drawable.b);
                imageView.setRotation(90);
                break;
            case 'b':
                imageView.getLayoutParams().height = (int) width;
                imageView.getLayoutParams().width = (int) width * 2;
                imageView.setImageResource(R.drawable.b);
                break;
            case 'c':
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 2;
                imageView.setImageResource(R.drawable.d);
                imageView.setRotation(180);
                break;
            case 'd':
                imageView.setMidX(-width);
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 2;
                imageView.setImageResource(R.drawable.d);
                break;
            case 'e':
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 2;
                imageView.setImageResource(R.drawable.d);
                imageView.setRotation(-90);
                break;
            case 'g':
                imageView.setMidY(-(2*width));
                imageView.getLayoutParams().height = (int) width * 3;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.g);
                break;
            case 'h':
                imageView.getLayoutParams().height = (int) width * 3;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.g);
                imageView.setRotation(90);
                break;
            case 'j':
                imageView.setMidX(-width);
                imageView.setRotation(90);
                //the rest is same as for 'k'
            case 'k':
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 2;
                imageView.setImageResource(R.drawable.k);
                break;
            case 'l':
                imageView.setMidY(-width);
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.l);
                break;
            case 'm':
                imageView.setRotation(90);
                imageView.setMidY(0.5f*width);
                imageView.setMidX(-0.5f*width);
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.l);
                break;
            case 'n':
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.l);
                imageView.setRotation(180);
                break;
            case 'o':
                imageView.setRotation(-90);
                imageView.setMidX(-1.5f*width);
                imageView.setMidY(0.5f*width);
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.l);
                break;
            case 'p':
                imageView.setMidX(-1.5f*width);
                imageView.setMidY(0.5f*width);
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.q);
                imageView.setRotation(-90);
                break;
            case 'r':
                imageView.setMidX(-1.5f*width);
                imageView.setMidY(0.5f*width);
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.q);
                imageView.setRotation(90);
                break;
            case 's':
                imageView.setRotation(180);
                // the rest is same as for 'q'
            case 'q':
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.q);
                break;
            case 'w':
                imageView.getLayoutParams().height = (int) width;
                imageView.getLayoutParams().width = (int) width;
                imageView.setImageResource(R.drawable.wall);
                break;
            case 'u':
                imageView.setRotation(180);
                // the rest is same as for 'z'
            case 'z':
                imageView.setMidY(-width);
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.z);
                break;
            case 'x':
                imageView.setMidX(-(0.5f*width));
                imageView.setMidY(0.5f *width);
                imageView.getLayoutParams().height = (int) width * 2;
                imageView.getLayoutParams().width = (int) width * 3;
                imageView.setImageResource(R.drawable.z);
                imageView.setRotation(90);
                break;
        }

        imageView.requestLayout();
        if (code != 'w')
            imageView.setOnTouchListener(listener);
        return imageView;
    }

    /**
     * Returns to the main menu.
     *
     * @param v pushed button
     */
    public void BackToMenu(View v) {
        Intent i = new Intent(this, MainMenu.class);
        startActivity(i);
    }

    /**
     * Starts the help.
     *
     * @param v pushed button
     */
    public void Help(View v) {
        Intent i = new Intent(this, Help.class);
        startActivity(i);
    }

    /**
     * Returns to the level selection menu.
     *
     * @param v pushed button
     */
    public void BackToSelect(View v) {
        finish();
    }

    /**
     * Restarts the current level.
     *
     * @param v pushed button
     */
    public void restart(View v) {
        FrameLayout layout = (FrameLayout) findViewById(R.id.layout);
        for (ImageView view : views) {
            layout.removeView(view);
        }
        loadGame();
    }

    /**
     * Starts the next level.
     *
     * @param v view
     */
    public void nextLevel(View v) {
        FrameLayout layout = (FrameLayout) findViewById(R.id.layout);
        for (ImageView view : views) {
            layout.removeView(view);
        }
        if (curGameNum < GameSelect.gameCount - 1) {
            curGameNum++;
            loadGame();
        } else {
          Intent i = new Intent(this, GameSelect.class);
          startActivity(i);
        }
    }
}
