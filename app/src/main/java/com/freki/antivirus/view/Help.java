package com.freki.antivirus.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ViewFlipper;

import com.freki.antivirus.R;

/**
 * Represetns the Help screen.
 */
public class Help extends Activity {

    /** ViewFlipper for changing pages of the help. */
    ViewFlipper viewFlipper;
    /** Property used for getting the direction of swipe. */
    float lastX;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        viewFlipper = (ViewFlipper) findViewById(R.id.flipper);
    }

    /**
     * The method for handling the touch events.
     * @param touchevent the touch event
     * @return true if event was handled, false otherwise
     */
    public boolean onTouchEvent(MotionEvent touchevent) {
        switch (touchevent.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                lastX = touchevent.getX();
                break;
            }
            case MotionEvent.ACTION_UP: {
                float currentX = touchevent.getX();

                if (lastX > currentX) {
                    if (viewFlipper.getDisplayedChild() == 5)
                        break;

                    viewFlipper.setInAnimation(this, R.animator.to_rigth);
                    viewFlipper.setOutAnimation(this, R.animator.to_left_out);
                    viewFlipper.showNext();
                } else if (lastX < currentX) {
                   if (viewFlipper.getDisplayedChild() == 0)
                        break;

                    viewFlipper.setInAnimation(this, R.animator.to_left);
                    viewFlipper.setOutAnimation(this, R.animator.to_rigth_out);
                    viewFlipper.showPrevious();
                }
                break;
            }
        }
        return false;
    }

    /**
     * Returns to previous activity.
     *
     * @param v pushed button
     */
    public void onCloseClick(View v) {
       finish();
    }
}
