package com.freki.antivirus.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import com.freki.antivirus.R;

/**
 * Represents the main menu. Is the launcher activity.
 */
public class MainMenu extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    /**
     * Starts the GameSelects activity.
     * @param v pushed button
     */
    public void onClickGame(View v) {
        Intent i = new Intent(this, GameSelect.class);
        startActivity(i);
    }

    /**
     * Launches the help.
     * @param v pushed button
     */
    public void onClickHelp(View v) {
        Intent i = new Intent(this, Help.class);
        startActivity(i);
    }

    /**
     * Shows the about dialog.
     * @param v pushed button
     */
    public void onClickAbout(View v) {
        AlertDialog a1 = new AlertDialog.Builder(this).create();
        a1.setTitle(R.string.about);
        a1.setMessage("Aplikace byla vytvořena jako projekt pro předmět KIV/MKZ na Fakultě aplikovaných věd Západočeské univerzity v Plzni.\n\nAutor: Kateřina Štollová\n(c) 2015");
        a1.setButton(RESULT_OK, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // nothing to do now
            }
        });
        a1.show();
    }
}
