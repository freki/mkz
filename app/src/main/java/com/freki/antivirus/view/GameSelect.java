package com.freki.antivirus.view;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.freki.antivirus.R;
import com.freki.antivirus.utils.ImageAdapter;

/**
 * Represents menu for game selection.
 */
public class GameSelect extends Activity {

    /**
     * Total count of levels.
     */
    final static int gameCount = 20;

    /**
     * Scroll position of last selected level.
     */
    private static int scrollY = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_select);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        GridView grid = (GridView) findViewById(R.id.gridView);
        final ImageAdapter adapter = new ImageAdapter(this);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
             scrollY = position;
             startGame(position);
            }
        });

        for (int i = 0; i < gameCount; i++) {
            if (i < 6)
                adapter.addImage(R.drawable.easy);
            else if (i < 14)
                adapter.addImage(R.drawable.medium);
            else
                adapter.addImage(R.drawable.hard);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        GridView grid = (GridView) findViewById(R.id.gridView);
        grid.smoothScrollToPosition(scrollY);
    }

    /**
     * Starts the selected game.
     * @param num the index of the selected game
     */
    private void startGame(int num) {
        Intent intent = new Intent(this, Game.class);
        Bundle b = new Bundle();
        b.putInt("gameNumber", num);
        intent.putExtras(b);
        startActivity(intent);
    }
}
